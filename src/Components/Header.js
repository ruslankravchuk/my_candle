import React, {
  Component
} from 'react'
import {
  Button,
  Container,
  Form,
  FormControl,
  Navbar,
  Nav
} from 'react-bootstrap'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
import Logo from './../img/logo.png'
import Home from "../Pages/Home"
import AboutUs from "../Pages/About"
import Contacts from "../Pages/Contacts"
import Blogs from "../Pages/Blogs"
import Exampls from "../Pages/Example"

export default class Header extends Component {
  render() {
    return (
      <>
        <Navbar collapseOnSelect expand="md" bg="dark" variant="dark">
          <Container>
            <Navbar.Brand href="/">
              < img
                src={Logo}
                weight="30"
                height="30"
                className="d-inline-block align-top"
                alt="Logo"
              />{""}
            React site
          </Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="mr-auto">
                <Nav.Link href="/">
                  Home
              </Nav.Link>
                <Nav.Link href="/About">
                  AboutUs
              </Nav.Link>
                <Nav.Link href="/Contacts">
                  Contacts
              </Nav.Link>
                <Nav.Link href="/Blogs">
                  Blogs
              </Nav.Link>
                <Nav.Link href="/Exampls">
                  Exampls
              </Nav.Link>
              </Nav>
              <Form inline>
                <FormControl
                  type="text"
                  placeholder="Search"
                  className="mr-sm-2"
                />
                <Button variant="outline-info">
                  Search
              </Button>
              </Form>
            </Navbar.Collapse>
          </Container>
        </Navbar>
        <Router>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/About" component={AboutUs} />
            <Route exact path="/Contacts" component={Contacts} />
            <Route exact path="/Blogs" component={Blogs} />
            <Route exact path="/Exampls" component={Exampls} />
          </Switch>
        </Router>
      </>

    )
  }
}
